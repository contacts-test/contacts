import Vue from 'vue'
import moment from "moment";
import accounting from "accounting";
import VuetableFieldSequence from 'vuetable-2/src/components/VuetableFieldSequence.vue'
import VuetableFieldCheckbox from 'vuetable-2/src/components/VuetableFieldCheckbox.vue'
Vue.component('vuetable-field-checkbox', VuetableFieldCheckbox)

export default [

    {
        name: 'id',
        title: "ID",
        width: "5%"
    },
    {
        name: "name",
        title: '<i class="grey user outline icon"></i>Name',
        width: "20%",
        sortField: "name"
    },
    {
        name: "email",
        title: '<i class="fa fa-envelope" aria-hidden="true"></i> Email',
        width: "20%"
    },
    {
        name: "gender-slot",
        title: '<i class="fa grey heterosexual icon"></i>Gender',
        titleClass: "center aligned",
        dataClass: "center aligned",
        width: "15%",
    },
    {
        name: "content",
        title: '<i class="grey money icon"></i>Content',
        titleClass: "center aligned",
        dataClass: "right aligned",
        width: "15%",
    },
    {
        name: "action-slot",
        title: '<i class="fa grey heterosexual icon"></i>Action',
        titleClass: "center aligned",
        dataClass: "center aligned",
        width: "15%",
    },

];
