require('./bootstrap');
require('admin-lte');
import Vue from 'vue';

// import DataTable from 'laravel-vue-datatable';
import 'bulma/css/bulma.css'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);
// Vue.use(DataTable);

Vue.component('Contact', require('./views/Contact.vue').default);
Vue.component('Edit', require('./views/Edit.vue').default);
Vue.component('Add', require('./views/Add.vue').default);


const app = new Vue({
    el: '#app',
});
