<?php

namespace Database\Seeders;

use App\Models\Contact;
use Illuminate\Database\Seeder;

class ContactTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Contact::updateOrCreate(
			[
				'id' => 1
			],
			[
				'name'    => 'Thembinkosi Fumba',
				'email'   => 'testuser@mail.com',
				'gender'  => 'Male',
				'content' => 'The standard Lorem Ipsum passage, used since the 1500s'
			]
		);

		Contact::updateOrCreate(
			[
				'id' => 2
			],
			[
				'name'    => 'John Doe',
				'email'   => 'doe@mail.com',
				'gender'  => 'Male',
				'content' => 'The standard Lorem Ipsum passage, used since the 1500s'
			]
		);
	}
}
