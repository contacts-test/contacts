<?php

namespace App\Http\Controllers;

use App\Repositories\ContactRepositoryInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ContactController extends Controller
{
	protected $contactRepository;

	public function __construct(ContactRepositoryInterface $contactRepository)
	{
		$this->contactRepository = $contactRepository;
	}

	public function index()
	{
		return view('Contact.Contact');
	}

    /**
     * @return Application|Factory|View
     */
	public function add()
	{
		return view('Contact.add');
	}

    /**
     * @param Request $request
     * @return array
     */
	public function getDataTableData(Request $request)
	{
		$length   = $request->input('length');
		$query    = $this->contactRepository->getDataTableData($request);
		$contacts = $query->paginate($length);
		return ['response' => $contacts, 'draw' => $request->input('draw')];
	}


    /**
     * @param $id
     * @return Application|Factory|View
     */
	public function getRow($id)
	{
		$row =  $this->contactRepository->getRow($id);
		return view('Contact.edit')
			->with(compact('row'));
	}

    /**
     * @param Request $request
     * @return string
     */
	public function save(Request $request)
	{
		$status = $this->contactRepository->update($request);
		return 'success';
	}

    /**
     * @param Request $request
     * @return string
     */
	public function store(Request $request)
	{
		$status = $this->contactRepository->store($request);
		return 'success';
	}

    /**
     * @param $id
     * @return string
     */
	public function delete($id)
	{
		$delete = $this->contactRepository->delete($id);
		return 'success';
	}
}
