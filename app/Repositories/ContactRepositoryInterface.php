<?php


namespace App\Repositories;


use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

interface ContactRepositoryInterface
{
	/**
	 * @return Collection|null
	 */
	public function all(): ?Collection;

	/**
	 * @param Request $request
	 * @return Builder|null
	 */
	public function getDataTableData(Request $request): ?Builder;

	/**
	 * @param $id
	 * @return Collection
	 */
	public function getRow($id): Collection;

    /**
     * @param Request $request
     * @return mixed
     */
	public function update(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
	public function store(Request $request);

    /**
     * @param $id
     * @return mixed
     */
	public function delete($id);
}
