<?php


namespace App\Repositories;


use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

class ContactRepository implements ContactRepositoryInterface
{
    protected $user;
    protected $contact;

	public function __construct(Contact $contact)
	{
		$this->contact = $contact;
	}

	/**
	 * @return Collection|null
	 */
	public function all(): ?Collection
	{
	   return $this->contact->all();
	}

	/**
	 * @param Request $request
	 * @return Builder|null
	 */
	public function getDataTableData(Request $request): ?Builder
	{
		$columns = ['id','name', 'email','gender', 'content'];
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = Contact::select('id', 'name', 'email','gender', 'content')->orderBy($columns[$column], $dir);

		if ($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('gender', 'like', '%' . $searchValue . '%')
					->orWhere('name', 'like', '%' . $searchValue . '%');
			});
		}

		return $query;
	}

    /**
     * @param $id
     * @return Collection
     */
	public function getRow($id): Collection
	{
		return Contact::where('id',$id)->get();
	}

    /**
     * @param Request $request
     * @return mixed
     */
	public function update(Request $request)
    {
        $contact = $this->contact->where('id',$request->row[0]['id'])
            ->update([
                'name'    => $request->row[0]['name'],
                'email'   => $request->row[0]['email'],
                'gender'  => ($request->gender == 1) ? 'Male' : 'Female',
                'content' => $request->row[0]['content'],
            ]);

        return $contact;
    }

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function store(Request $request)
	{
		$contact = $this->contact->create([
			'name'    => $request->row['name'],
			'email'   => $request->row['email'],
			'gender'  => ($request->row['gender'] == 1) ? 'Male' : 'Female',
			'content' => $request->row['content'],
		]);

		return $contact;
	}

	public function delete($id)
	{
		$delete = $this->contact->where('id',$id)->delete();

		return $delete;
	}
}
