<?php

use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();


Route::get('/', [ContactController::class, 'index'])->name('contact')->middleware('auth');
Route::get('/contacts', [ContactController::class, 'getDataTableData'])->name('dataTable')->middleware('auth');
Route::get('/edit-contacts/{id}', [ContactController::class, 'getRow'])->name('dataTable')->middleware('auth');
Route::get('/add-contacts', [ContactController::class, 'add'])->name('add')->middleware('auth');
Route::put('/edit-save', [ContactController::class, 'save'])->name('save')->middleware('auth');
Route::post('/store', [ContactController::class, 'store'])->name('store')->middleware('auth');
Route::post('/contact-delete/{id}', [ContactController::class, 'delete'])->name('delete')->middleware('auth');
